window.$ = window.jQuery = require('jquery')
require('holderjs');
var bootstrap = require('bootstrap-sass');

$(document).ready(function() {
  console.log('init');

  $('div.modal').on('shown.bs.modal', function (e) {
    $(this).find('input[autofocus]').focus();
  });

  $('.launch-modal').on('click', function(e){
    e.preventDefault();
    $( '#' + $(this).data('modal-id') ).modal();
  });
});
