<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@yield('title', 'HTML Editor')</title>

  <link rel="stylesheet" href="{{ elixir('assets/app.css') }}">

  @if(is_admin())
    <link rel="stylesheet" href="/content-tools/content-tools.min.css">
  @endif

  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Project name</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right">
          <div class="form-group">
            <input type="text" placeholder="Email" class="form-control">
          </div>
          <div class="form-group">
            <input type="password" placeholder="Password" class="form-control">
          </div>
          <button type="submit" class="btn btn-success">Sign in</button>
        </form>
      </div><!--/.navbar-collapse -->
    </div>
  </nav>

  @yield('content')

  <div class="container">
    <hr>

    <footer>
      <p>&copy; 2015 Company, Inc.</p>
    </footer>
  </div> <!-- /container -->

  @stack('dialogs')

  <script type="text/javascript" src="{{ elixir('assets/app.js') }}"></script>

  @if(is_admin())
    <script src="/content-tools/content-tools.min.js"></script>
    <script src="/content-tools/editor.js"></script>
  @endif

  @stack('scripts')
</body>
</html>
