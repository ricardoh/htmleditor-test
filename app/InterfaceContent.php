<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterfaceContent extends Model
{
    protected $fillable = [
        'key', 'value',
    ];
}
