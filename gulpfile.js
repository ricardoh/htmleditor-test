var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/fonts/');
    mix.copy('resources/assets/fonts', 'public/fonts/');

    mix.sass('app.scss', 'public/assets/app.css');

    mix.browserify([
      'app.js',
      '../vendors/ie10-viewport-bug-workaround.js'
    ], 'public/assets/app.js');

    mix.version([
      'assets/app.css',
      'assets/app.js'
    ]);
});
